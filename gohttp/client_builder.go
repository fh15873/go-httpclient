package gohttp

import (
	"net/http"
	"time"
)

// NewBuilder builds config of HTTP Client and It must be called before any HTTP request is executed.
//
// Returns ClientBuilder to sets configuration exposed by [ClientBuilder].
func NewBuilder() ClientBuilder {
	builder := &clientBuilder{}
	return builder
}

func (c *clientBuilder) Build() Client {
	client := httpClient{
		builder: c,
	}
	return &client
}

func (c *clientBuilder) SetHeaders(headers http.Header) ClientBuilder {
	c.headers = headers
	return c
}

func (c *clientBuilder) DisableTimeouts(disable bool) ClientBuilder {
	c.disableTimeouts = disable
	return c
}

func (c *clientBuilder) SetConnectionTimeout(timeout time.Duration) ClientBuilder {
	c.connectionTimeout = timeout
	return c
}

func (c *clientBuilder) SetResponseTimeout(timeout time.Duration) ClientBuilder {
	c.responseTimeout = timeout
	return c
}

func (c *clientBuilder) SetMaxIdleConnections(connections int) ClientBuilder {
	c.maxIdleConnections = connections
	return c
}

func Add(n1 int, n2 int) int {
	return n1 + n2
}

type clientBuilder struct {
	headers            http.Header
	maxIdleConnections int
	connectionTimeout  time.Duration
	responseTimeout    time.Duration
	disableTimeouts    bool
}

// ClientBuilder methods set configuration for HTTP client
type ClientBuilder interface {
	// SetHeaders sets global headers for the client.
	// They can be override by Get,Put,Post,Delete methods
	SetHeaders(headers http.Header) ClientBuilder

	// SetConnectionTimeout sets the connection timeout for the client.
	// It actually sets Dialer.timeout https://pkg.go.dev/net#Dialer
	// timeout - use default - '1 * time.Second' if timeout is less than or equal to 0.
	SetConnectionTimeout(timeout time.Duration) ClientBuilder

	// SetResponseTimeout sets the response timeout for the client.
	// It actually sets https://cs.opensource.google/go/go/+/refs/tags/go1.20.5:src/net/http/transport.go
	// timeout - use default - '5 * time.Second' if timeout is less than or equal to 0.
	SetResponseTimeout(timeout time.Duration) ClientBuilder

	// SetMaxIdleConnections sets the maximum number of idle connections for the client.
	// connections - Use default - 5 if connections is less than or equal to 0.
	SetMaxIdleConnections(connections int) ClientBuilder

	DisableTimeouts(disable bool) ClientBuilder

	// Build creates a client based on the configuration.
	// It sets builder and exposed HTTP verbs mentioned in Client
	Build() Client
}
