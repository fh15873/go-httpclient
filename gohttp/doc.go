// Package gohttp provides HTTP client and server implementations.
//
// Make HTTP Get, Put, Post, and Delete requests.
package gohttp
