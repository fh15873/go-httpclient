package gohttp

import (
	"encoding/json"
	"net/http"
)

func (r *Response) Bytes() []byte {
	return r.Body
}

func (r *Response) String() string {
	return string(r.Body)
}

func (r *Response) UnmarshalJson(target interface{}) error {
	return json.Unmarshal(r.Bytes(), target)
}

type Response struct {
	Status     string
	StatusCode int
	Headers    http.Header
	Body       []byte
}
