package gohttp_test

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/fh15873/go-httpclient/gohttp"
	"gitlab.com/fh15873/go-httpclient/gomime"
)

func ExampleNewBuilder() {
	gohttp.NewBuilder()
	fmt.Println("New builder instance is created")

	//Output:New builder instance is created
}
func ExampleAdd() {
	sum := gohttp.Add(2, 3)

	fmt.Println(sum)

	// Output: 5
}
func ExampleClientBuilder_SetHeaders() {
	headers := make(http.Header)
	headers.Set("Content-Type", "application/json")

	builder := gohttp.NewBuilder()
	builder.SetHeaders(headers)

	fmt.Println("Headers set successfully.")

	// Output: Headers set successfully.
}

func ExampleClient_Get() {
	// Create a channel to receive the result of the asynchronous operation
	resultChan := make(chan int)

	// Perform the asynchronous operation in a goroutine
	go func() {
		// Simulate some time-consuming operation
		headers := make(http.Header)
		headers.Set(gomime.HeaderContentType, gomime.ContentTypeJson)

		client := gohttp.NewBuilder().
			SetHeaders(headers).
			SetConnectionTimeout(2 * time.Second).
			SetResponseTimeout(3 * time.Second).Build()

		response, err := client.Get("https://api.github.com", nil)
		if err != nil {
			panic(err)
		}

		// Send the result to the channel
		resultChan <- response.StatusCode
	}()

	// Wait for the result from the channel
	result := <-resultChan

	// Print the result
	fmt.Println(result)

	// Output: 200
}
