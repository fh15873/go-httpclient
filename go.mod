module gitlab.com/fh15873/go-httpclient

go 1.20

require (
	github.com/yuin/goldmark v1.4.13 // indirect
	golang.org/x/mod v0.11.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
	golang.org/x/tools v0.10.0 // indirect
)
