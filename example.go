package main

import (
	"fmt"

	"gitlab.com/fh15873/go-httpclient/gohttp"
)

var (
	githubHttpClient = getGithubClient()
)

func getGithubClient() gohttp.Client {
	builder := gohttp.NewBuilder().DisableTimeouts(true).Build()

	return builder
}

func main() {
	GetUrls()
}

func GetUrls() {
	response, err := githubHttpClient.Get("https://api.github.com", nil)
	if err != nil {
		panic(err)
	}

	fmt.Println(response.StatusCode)
}

type User struct {
	FirstName string `json:"first_name" xml:"FIRST_NAME"`
	LastName  string `json:"last_name" xml:"LAST_NAME"`
}
